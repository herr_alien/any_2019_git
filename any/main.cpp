#include <functional>
#include <iostream>
#include "any.h"

#define TEST_DATA_ARRAY_LEN 100

class TestData
{
public: 
    TestData() 
    { 
        m_data = new int[TEST_DATA_ARRAY_LEN];
    }
    ~TestData()
    {
        if (nullptr != m_data)
        {
            delete[] m_data;
        }
    }

    TestData& operator=(const TestData& t)
    {
        this->~TestData();
        m_data = new int[TEST_DATA_ARRAY_LEN];
        memcpy(m_data, t.m_data, TEST_DATA_ARRAY_LEN * sizeof(int));
        return *this;
    }

    TestData(const TestData& t)
    {
        m_data = nullptr;
        this->operator=(t);
    }

private:
    int * m_data;

};

int main()
{
    {
        Any a;
        TestData t;
        a = t;
    }

    {
        Any a, b;
        int va = 3;
        double vb = 3.5;
        a = va;
        b = vb;
        a = b;
        std::cout << "Assignment any = any: " << (a.get<double>() == 3.5 ? "PASSED" : "FAILED") << std::endl;
    }

    {
        Any a;
        int val1 = 2, val2 = 7;
        a = val1;
        a = val2;
        std::cout << "Multiple int assignments: " << (a.get<int>() == val2 ? "PASSED" : "FAILED") << std::endl;
    }

    {
        Any a;
        int val1 = 2;
        a = val1;
        std::cout << "get for invalid type: ";
        try
        { 
            a.get<double>();
            std::cout << "FAILED"; // get should throw
        }
        catch (...)
        {
            std::cout << "PASSED";
        }
        std::cout << std::endl;
    }

    return 0;
}
