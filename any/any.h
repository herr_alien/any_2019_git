#pragma once

#include <functional>
#include <typeinfo>

typedef void(*deleterFunction)(void*);

template <typename T>
void templateDeleter(void * p)
{
    T* asT = static_cast<T*> (p);
    delete asT;
}

typedef void(*copierFunction)(void*&, void*); 
template <typename T>
void copyFunction(void *& dest, void * src)
{
    T* srcAsT = (T*)src;
    T* destAsT = new T(*srcAsT);
    dest = destAsT;
}

class Any
{
public:
	Any() {
		m_data = nullptr;
        m_dataTypeInfo = nullptr;
	}
	template <typename T>
	T get() {
        if (&typeid(T) != m_dataTypeInfo)
        {
            throw "Invalid type!";
        }

		T* tVal = static_cast<T*>(m_data);
		return *tVal;
	}
	
    template <typename T>
	Any& operator=(const T& val) {

        if (&typeid(T) != m_dataTypeInfo)
        {
            this->~Any();

            //deleter = templateDeleter<T>;
            deleter = [](void* p) {
                T* cast = static_cast<T*>(p);
                delete cast;
            };
            copier = copyFunction<T>;
            m_data = new T(val);
            m_dataTypeInfo = &typeid(T);
        }
        else
        {
            T* ownDataCast = static_cast<T*>(m_data);
            *ownDataCast = val;
        }
        return *this;
    }

    Any& operator= (const Any & other) {
        this->~Any();

        other.copier(m_data, other.m_data);
        copier = other.copier;
        deleter = other.deleter;
        m_dataTypeInfo = other.m_dataTypeInfo;

        return *this;
    }

    Any(const Any & other) {
        m_data = nullptr;
        this->operator=(other);
    }

    ~Any()  {
        if (nullptr != m_data)
            deleter( m_data);
        m_dataTypeInfo = nullptr;
    }
private:
	void * m_data;
    std::function <void(void*)> deleter;
    
    // we can use also std::function <void(void*,void*)>
    // instead of the function pointer typedef
    copierFunction copier;

    // type_info is has static storage, so don't delete it!
    const std::type_info * m_dataTypeInfo;

};



